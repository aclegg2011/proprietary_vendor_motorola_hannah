#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:19724288:140092b80355114849edb0c9a896fdfa5bda556c; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:12472320:e0cc5716b77a5943ba75c8631c349d33669bf6f1 EMMC:/dev/block/bootdevice/by-name/recovery 140092b80355114849edb0c9a896fdfa5bda556c 19724288 e0cc5716b77a5943ba75c8631c349d33669bf6f1:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
